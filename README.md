# Developer Advocacy Organisation Project

Welcome to the teams' Meta project, which houses issues & issue boards the team works with. Please follow the documented workflows in the [handbook](https://about.gitlab.com/handbook/marketing/developer-relations/developer-evangelism/) to [request team support](https://about.gitlab.com/handbook/marketing/developer-relations/developer-evangelism/#want-to-work-with-the-team): Speakers, events, CFPs, community response plans, etc. You can also find us on [Slack](https://about.gitlab.com/handbook/marketing/developer-relations/developer-evangelism/#find-us-on-slack).

You can use the Issue templates listed below to create issues for your request or activity. 

### Issue Templates

- [Request a Developer Advocate](https://gitlab.com/gitlab-com/marketing/developer-relations/developer-advocacy/developer-advocacy-meta/-/issues/new?issuable_template=developer-advocate-request)
- [Everyone can Contribute Cafe insight](https://gitlab.com/gitlab-com/marketing/developer-relations/developer-advocacy/developer-advocacy-meta/-/issues/new?issuable_template=everyonecancontribute-cafe-insights)
- [Stable Countterpart Request](https://gitlab.com/gitlab-com/marketing/developer-relations/developer-advocacy/developer-advocacy-meta/-/issues/new?issuable_template=dev-advocacy-stablecounterpart)
- [Community Response Plan](https://gitlab.com/gitlab-com/marketing/developer-relations/developer-advocacy/developer-advocacy-meta/-/issues/new?issuable_template=community-response-plan)
- [Event Strategy](https://gitlab.com/gitlab-com/marketing/developer-relations/developer-advocacy/developer-advocacy-meta/-/issues/new?issuable_template=event_strategy)
- [Code Challenge Checklist](https://gitlab.com/gitlab-com/marketing/developer-relations/developer-advocacy/developer-advocacy-meta/-/issues/new?issuable_template=codechallenge-checklist)

## Credits

Project avatar photo by <a href="https://unsplash.com/fr/@claybanks?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Clay Banks</a> on <a href="https://unsplash.com/photos/LjqARJaJotc?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>. 
   
