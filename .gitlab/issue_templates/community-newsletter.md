<!-- This is an issue template for the Community Newsletter: https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/operations/#community-newsletter -->

<!-- Please use the format below to add an Issue title: -->
<!-- Community Newsletter: Issue <Num>, <Month>, Content Due: <Content Due Date>, Send: <Send Due Date> -->

This issue compiles the draft for: 

* `Issue`: 
* `Month`: 
* `Content Due Date`: 
* `Send Due Date`: 
* `Feedback Due Date`: 

<!-- The draft gets compiled in mailjet -->

### Social Copy 
<!-- You can optionally create some highlights for social to share -->
* `Highlights/insights for social copy.`

## Submissions
To submit content for the newsletter, drop a comment with the following information:

```
* `Title`: 
* `Text`:
* `CTA Link (optional)`:
* `Media Asset (optional)`: 
```

## Feedback
To provide feedback on the newsletter, drop a comment:

```
* :thumbs-up: something that is working well
* :speech_balloon: something can be better
* :bulb: a new idea or potential feature
```

### Checklist:
* [ ] Content reminder
* [ ] Draft content
* [ ] Generate important links with Campaign Manager
* [ ] Editorial review
* [ ] Draft social highlight
* [ ] Add `Community Newsletter::Ready` if ready for final review + send.
* [ ] Schedule for send in Mailjet
* [ ] Add `Community Newsletter::Feedback` after send to collect feedback and add a week to due date.
* [ ] Capture Mailjet performance data and review feedback before closing the issue.

FYI @gitlab-com/marketing/developer-relations 

<!-- Please leave the labels below on this issue -->
/label ~"Community Newsletter" ~"Community Newsletter::Draft" ~"Developer Relations" ~"developer-advocacy" ~"DA-Type:Content" ~"DA-Status:ToDo"
/assign @sugaroverflow
