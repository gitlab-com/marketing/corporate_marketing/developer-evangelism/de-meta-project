<!-- **PLEASE READ CAREFULLY AND FILL IN ALL FIELDS!!!** -->

## Product tour proposal
(What will your product tour be about? Please be as specific as possible in your pitch here.)



### Which best describes your tour? (Please check at least one box.)
- [ ] new feature/capability
- [ ] tutorial/how-to for a feature/capability
- [ ] tour for conference
- [ ] other - please explain

### Does your product tour fit into one of the following priority areas?
- [ ] AI/ML
- [ ] security and compliance
- [ ] CI/CD
- [ ] competitive play
- [ ] public sector
- [ ] developer environments
- [ ] enterprise Agile planning
- [ ] other

### Is this tour time-sensitive to a product launch, feature announcement, campaign, or other important event?
- [ ] yes (if yes, what is the target publish date?)
- [ ] no



## Next steps
The DA team will review your pitch and provide feedback. Once your pitch is approved, please draft your narrative using this Google Docs [product tour template](https://docs.google.com/document/d/1Et_YH8JS74niWDKKxeeOYqFyhTzMsEyHoA5yOP4HxU8/edit?usp=sharing). When your draft is ready for review, please add the Google Doc link in a comment in this issue.

## Questions?
Please tag @iganbaruch with any questions.

---

<!-- these labels should be included on all templates -->
/label ~"Department::Developer Relations" ~"developer-advocacy" 


<!-- Content Type, we will use product tour as the default, you can see other types in https://gitlab.com/groups/gitlab-com/-/labels?subscribed=&sort=relevance&search=DA-Content-Type -->

/label ~"DA-Type-Content::product-tour" ~"DA-Type::Content" ~"FY26-Q1" ~"DA-Status::ToDo"

<!-- Mention team members that should be aware of the content request. -->
/cc @iganbaruch 
