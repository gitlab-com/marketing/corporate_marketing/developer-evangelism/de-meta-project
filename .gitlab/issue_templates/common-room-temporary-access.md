<!-- This issue template is for requesting temporary access to Common Room --> 
<!-- Suggested Title: Common Room - temporary seat expiration --> 

This issue tracks the de-provisioning of a temporary access account on Common Room:

- gitlab email of user: ``
- link to access request: 

### Tasks for provisioner:

- [ ] send email to schedule time with Common Room 
- [ ] add expiration as due date on issue (3 months from provisioning date)

<!-- Keep assignees. --> 
/assign @sugaroverflow 

/cc @johncoghlan

<!-- Keep confidential. --> 
/confidential 

<!-- Keep the labels. -->
~"DA-Status::ToDo" 